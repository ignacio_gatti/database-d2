
USE master
GO
drop database D2_9935621
CREATE DATABASE D2_9935621
USE D2_9935621


CREATE TABLE clients(
client_username varchar (30) primary key not null,
password varchar (30) not null,
sname varchar (30) not null,
fname varchar (30) not null,
email varchar (30) not null,
phone varchar (30) not null,
Check (email like '%@%.%')
)


CREATE TABLE type(
name varchar (30) primary key not null,
cost float not null,
hours int not null
)


CREATE TABLE admin(
username varchar(30) primary key not null,
password varchar(30) not null,
sname varchar(30) not null,
fname varchar(30) not null,
email varchar(30) not null,
Check (email like '%@%.%')
)


CREATE TABLE document(
id int primary key identity (1,1) not null ,
date int not null,
link varchar(60) not null,
type varchar(30) not null,
check (type like 'bill' OR type like 'document')

)

CREATE TABLE timeslot(
tid int not null,
time varchar(50) not null,
date DATE not null,

primary key (tid, time, date)

)

CREATE TABLE cars(
license varchar(30) primary key not null,
make varchar(30) not null
)

CREATE TABLE instructor(
instructor_username varchar (30) primary key not null,
password varchar (30) not null,
sname varchar (30) not null,
fname varchar (30) not null,
email varchar (30) not null,
phone varchar (30) not null,
Check (email like '%@%.%')

)

CREATE TABLE appointment(
id int not null,
notes varchar(30),

tid int not null,
time varchar(50) not null,
date DATE not null,
client_username varchar(30) not null,
license varchar(30) not null,
instructor_username varchar(30)

primary key(id),
Foreign Key (tid, time, date) references timeslot,
Foreign Key (client_username) references clients,
Foreign Key (license) references cars, 
Foreign Key (instructor_username) references instructor 
)




INSERT INTO cars VALUES('V4FFER', 'Toyota')
INSERT INTO cars VALUES('HURY25', 'Renault')
INSERT INTO cars VALUES('598KHJ', 'Toyota')
INSERT INTO cars VALUES('PPIL88', 'Volkswagen')
INSERT INTO cars VALUES('LL99362', 'Renault')


INSERT INTO type VALUES('New Customer',39.99,5)
INSERT INTO type VALUES('Experienced Customer',19.99,2)


INSERT INTO instructor Values ('johnv', 'johnpw', 'Vox', 'John', 'johnv@gmail.com', '555-555')
INSERT INTO instructor Values ('maxl', 'maxpw', 'Landis', 'Max', 'maxl@outlook.com', '555-888')
INSERT INTO instructor Values ('bradp', 'bradpw', 'Pitt', 'Brad', 'bradp@gmail.com', '557-082')
INSERT INTO instructor Values ('mattd', 'mattpw', 'Damon', 'Matt', 'mattd@gmail.com', '588-666')


INSERT INTO admin VALUES('jackj', 'jackpw','Johnson','Jack','jackj@gmail.com')
INSERT INTO admin VALUES('katyp', 'katypw','Perry','Katy','katyp@outlook.com')
INSERT INTO admin VALUES('bobm', 'bobpw','Marley','Bob','bobm@gmail.com')


--https://blog.sqlauthority.com/2007/10/24/sql-server-simple-example-of-while-loop-with-continue-and-break-keywords/

--DO WHILE loop to automatically fill in the timeslot days and hours 
DECLARE @day INT
SET @day = 1
WHILE (@day <= 30)
BEGIN
		
		DECLARE @hour INT
		DECLARE @endOfHour INT
		SET @hour = 7
		SET @endOfHour = 8
		
	WHILE (@hour <= 19) 
	BEGIN
		
		
		
		INSERT INTO timeslot VALUES (1, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))
		INSERT INTO timeslot VALUES (2, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))
		INSERT INTO timeslot VALUES (3, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))
		INSERT INTO timeslot VALUES (4, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))

	
		SET @hour = @hour + 1
		SET @endOfHour = @hour + 1
	END
	
	SET @day = @day + 1
	
		IF @day = 7 
			SET @day = @day + 1

		ELSE IF @day = 14
			SET @day = @day + 1

		ELSE IF @day = 21
			SET @day = @day + 1

		ELSE IF @day = 28
			SET @day = @day + 1
END

/* 
SELECT * FROM type
SELECT * FROM document
SELECT * FROM appointment
SELECT * FROM timeslot
SELECT * FROM cars

SELECT * FROM instructor
SELECT * FROM admin
SELECT * FROM clients 

*/